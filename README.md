# To-do List

## 🖥️ Demo

**Acessar demonstração**: [![Vercel](https://img.shields.io/badge/vercel-%23000000.svg?style=for-the-badge&logo=vercel&logoColor=white)](https://to-do-list-marcosken.vercel.app/)

## 💡 Sobre o Projeto

Esse projeto tinha como objetivo criar uma aplicação no qual o usuário pudesse organizar suas tarefas em uma lista de afazeres. Trata-se de uma aplicação simples que utiliza conceitos básicos, porém, fundamentais tanto do React JS quanto do JavaScript, como a manipulação de states, props, uso de componentização, callbacks, spread operators e eventos de usuário.

Na aplicação, o usuário pode adicionar uma tarefa à lista de afazeres, digitando-a no campo específico e em seguida clicando no botão adicionar. Com as tarefas já adicionadas, o usuário pode gerenciá-las, clicando no checkbox ao lado de cada tarefa que ele concluir. Dessa forma, o texto indicando aquela tarefa será exibido de forma tachada. O usuário também pode excluir uma tarefa de forma individual, caso, por exemplo tenha cometido algum erro ao escrevê-la, bem como limpar toda a lista de tarefas para criar novas tarefas.

## ⚙️ Funcionalidades

- [x] Adição de tarefa à lista de afazeres
- [x] Gerenciamento de tarefas
- [x] Exclusão de tarefa individual
- [x] Exclusão de toda a lista de tarefas

## ⚒️ Conhecimentos aplicados

• [React JS](https://pt-br.reactjs.org/)
• [Styled Components](https://styled-components.com/)

## 👨‍💻 Autor

<img style="border-radius: 15%;" src="https://gitlab.com/uploads/-/system/user/avatar/8603970/avatar.png?width=400" width="70px;" alt=""/>

Marcos Kenji Kuribayashi

[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/marcos-kuribayashi/) [![Gmail Badge](https://img.shields.io/badge/-marcosken13@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white)](mailto:marcosken13@gmail.com)
[![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=flat-square&logo=github&logoColor=white)](https://github.com/kenmarcos) [![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=flat-square&logo=gitlab&logoColor=white)](https://gitlab.com/marcosken)

---

Desenvolvido por Marcos Kenji Kuribayashi 😉
