import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        list-style: none;
    }

    :root {
        --primary: #FF7F50;
        --primary-variant: #D55B2F;
        --white: #ffffff;
        --black: #0B090A;
        --background: #FFF5EE;
        --dark-gray: #666665;
        --font-title: 'Ubuntu', sans-serif;
        --font-body: 'Oxygen', sans-serif;
    }

    body {
        font-family: var(--font-body);
        font-size: 1rem;
    }

    h1, h2, h3 {
        font-family: var(--font-title);
    }

    button {
        border: none;
        cursor: pointer;
    }
`;

export default GlobalStyle;
