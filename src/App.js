import GlobalStyle from "./styles/global";
import { useState } from "react";
import TodoList from "./components/TodoList";
import Header from "./components/Header";
import { Container, Main, Section } from "./App.styles";
import TodoInput from "./components/TodoInput";
import { ModalProvider } from "styled-react-modal";

function App() {
  const [todos, setTodos] = useState([]);

  const addTodo = (newTodo) => {
    setTodos((state) => [...todos, newTodo]);
  };

  const deleteTodo = (todoIndex) => {
    setTodos(todos.filter((_, index) => index !== todoIndex));
  };

  const clearAll = () => {
    setTodos((state) => []);
  };

  return (
    <Container>
      <ModalProvider>
        <GlobalStyle />
        <Header />
        <Main>
          <Section>
            <h2>Tarefas do dia</h2>
            <TodoList todos={todos} deleteTodo={deleteTodo} />
            <TodoInput todos={todos} addTodo={addTodo} clearAll={clearAll} />
          </Section>
        </Main>
      </ModalProvider>
    </Container>
  );
}

export default App;
