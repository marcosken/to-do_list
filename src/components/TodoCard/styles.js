import styled from "styled-components";

export const Container = styled.div`
  border-bottom: 0.2px solid var(--dark-gray);
  min-height: 4rem;
  display: flex;
  padding: 10px;

  input {
    align-self: center;
    width: 1.5rem;
    height: 1.5rem;
    cursor: pointer;
  }

  span {
    flex: 1;
    word-break: break-all;
    align-self: center;
    margin: 0 10px 0 10px;
    text-decoration: ${(props) => (props.isChecked ? "line-through" : "none")};
    color: var(--primary-variant);
  }

  div {
    align-self: center;
  }
`;
