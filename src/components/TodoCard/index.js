import { Container } from "./styles";
import Button from "../Button";
import { FaRegTrashAlt } from "react-icons/fa";
import { useState } from "react";

const TodoCard = ({ todo, index, deleteTodo }) => {
  const [isChecked, setIsChecked] = useState(false);

  return (
    <Container isChecked={isChecked}>
      <input
        type="checkbox"
        onChange={(e) => setIsChecked((state) => e.target.checked)}
      />
      <span>{todo}</span>
      <div>
        <Button className="deleteTodo" onClick={() => deleteTodo(index)}>
          <FaRegTrashAlt />
        </Button>
      </div>
    </Container>
  );
};

export default TodoCard;
