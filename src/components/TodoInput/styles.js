import styled from "styled-components";
import Modal from "styled-react-modal";

export const Container = styled.div`
  border-top: 2px dashed var(--dark-gray);

  input {
    padding: 8px;
    border: none;
    outline: none;
    width: 100%;
    font-family: var(--font-body);
    font-size: 1rem;
  }

  div {
    display: flex;
    justify-content: space-between;
    padding: 8px;
    gap: 4rem;
  }
`;

export const StyledModal = Modal.styled`
  width: 20rem;
  height: 10rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 1.5rem;
  background-color: var(--background);
  border-radius: 8px;
  font-size: 1rem;
  span {
    text-align: center;
  }
  div {
    width: 100%;
    display: flex;
    justify-content: space-evenly;
  }
`;
