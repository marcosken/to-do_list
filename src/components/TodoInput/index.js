import { useState } from "react";
import { Container, StyledModal } from "./styles";
import Button from "../../components/Button";

const TodoInput = ({ todos, addTodo, clearAll }) => {
  const [todoInput, setTodoInput] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const handleInput = () => {
    if (todoInput !== "") {
      addTodo(todoInput);
      setTodoInput("");
    }
  };

  const handleClear = () => {
    clearAll();
    toggleModal();
  };

  function toggleModal(e) {
    setIsOpen(!isOpen);
  }

  return (
    <Container>
      <input
        type="text"
        value={todoInput}
        placeholder="Digite aqui sua tarefa..."
        onChange={(e) => setTodoInput((state) => e.target.value)}
      />
      <div>
        <Button className="addBtn" onClick={handleInput}>
          Adicionar
        </Button>
        <Button className="clearBtn" disabled={!todos[0]} onClick={toggleModal}>
          Limpar
        </Button>
      </div>
      <StyledModal isOpen={isOpen} onBackgroundClick={toggleModal}>
        <span>
          Tem certeza que quer excluir <strong>todas</strong> as tarefas?
        </span>
        <div>
          <Button className="yesBtn" onClick={handleClear}>
            Sim
          </Button>
          <Button className="noBtn" onClick={toggleModal}>
            Não
          </Button>
        </div>
      </StyledModal>
    </Container>
  );
};

export default TodoInput;
