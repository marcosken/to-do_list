import styled from "styled-components";

export const Container = styled.header`
  background-color: var(--primary);
  height: 4rem;

  h1 {
    color: var(--white);
    text-align: center;
    line-height: 4rem;
  }
`;
