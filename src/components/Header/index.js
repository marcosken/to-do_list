import { Container } from "./styles";

const Header = () => {
  return (
    <Container>
      <h1>To-do List</h1>
    </Container>
  );
};

export default Header;
