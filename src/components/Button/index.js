import { Container } from "./styles";

const Button = ({ children, className, ...rest }) => {
  return (
    <Container className={className} {...rest}>
      {children}
    </Container>
  );
};

export default Button;
