import styled from "styled-components";

export const Container = styled.button`
  &.addBtn,
  &.clearBtn,
  &.yesBtn,
  &.noBtn {
    border-radius: 8px;
    padding: 8px;
    font-size: 1rem;
    font-family: var(--font-body);
    font-weight: bold;
    width: 50%;
    &:hover {
      transform: translateY(-2px);
      filter: brightness(120%);
    }
  }

  &.addBtn {
    background-color: var(--primary-variant);
    color: var(--white);
  }

  &.clearBtn,
  &.yesBtn {
    background-color: var(--white);
    color: var(--primary-variant);
    border: 4px solid var(--primary-variant);
  }

  &.clearBtn {
    opacity: ${(props) => props.disabled && "0.5"};
    &:hover {
      transform: ${(props) => props.disabled && "none"};
      filter: ${(props) => props.disabled && "none"};
    }
  }

  &.deleteTodo {
    background-color: var(--white);
    border: 2px solid var(--dark-gray);
    border-radius: 50%;
    svg {
      font-size: 1.6rem;
      padding: 4px;
      color: var(--dark-gray);
      &:hover {
        color: var(--primary-variant);
      }
    }
    &:hover {
      border-color: var(--primary-variant);
      transform: translateY(-2px);
    }
  }

  &.yesBtn,
  &.noBtn {
    margin: 6px;
    width: 45%;
  }

  &.noBtn {
    background-color: red;
    color: var(--white);
  }
`;
