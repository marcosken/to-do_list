import TodoCard from "../TodoCard";
import { Container, MessageContainer } from "./styles";

const TodoList = ({ todos, deleteTodo }) => {
  return (
    <>
      {!!todos[0] ? (
        <Container>
          {todos.map((todo, index) => (
            <li key={index}>
              <TodoCard todo={todo} index={index} deleteTodo={deleteTodo} />
            </li>
          ))}
        </Container>
      ) : (
        <MessageContainer>
          <span>Você ainda não tem tarefas.</span>
          <small>Adicione suas tarefas e organize o seu dia!</small>
        </MessageContainer>
      )}
    </>
  );
};

export default TodoList;
