import styled from "styled-components";

export const Container = styled.ul`
  flex: 1;
  padding-bottom: 1rem;
`;

export const MessageContainer = styled.p`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: var(--dark-gray);
  span {
    font-size: 1.5rem;
  }
`;
