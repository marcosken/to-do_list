import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

export const Main = styled.main`
  background-color: var(--background);
  flex: 1;
  display: flex;
  justify-content: center;
`;

export const Section = styled.section`
  background-color: var(--white);
  border-radius: 8px;
  width: 90vw;
  box-shadow: 0px 0px 6px 2px var(--dark-gray);
  display: flex;
  flex-direction: column;
  margin: 2rem 0 2rem 0;

  h2 {
    color: var(--primary);
    border-bottom: 4px solid var(--primary);
    padding: 10px;
    text-align: center;
  }

  @media (min-width: 768px) {
    width: 50vw;
  }

  @media (min-width: 1024px) {
    width: 40vw;
  }
`;
